package scotlandyard;

import java.util.List;

/**
 * This is the component of the composite design pattern
 */
public interface User {

    /**The colour of (this) user.
     *
     * @return the colour of a (this) user.
     */
    public Colour getColour();

    /**The player attribute for this user
     *
     * @return the player of (this) user
     */
    public Player getPlayer();

    /** This function sets the location of (this) player.
     *
     * @param location the integer for the node where this player will be "set"
     */
    public void setLocation(Integer location);

    /** This function will return the location of (this) player.
     *
     * @return the integer for the location of (this) player.
     */
    public Integer getLocation();

    /** Return the number of a certain type of ticket.
     *
     * @param ticket the type of ticket requested.
     * @return the integer for the number of types this user has.
     */
    public Integer getNumberOfTickets(Ticket ticket);

    /** Returns a list of moves that a detective can make.
     *
     * @param graph this consists of the nodes and edges of the game.
     * @return A list of moves that this detective can make.
     */
    public List<Move> getValidMoves(ScotlandYardGraph graph);

    /**
     *  Plays a move.
     * @param move A move that is played.
     */
    public void play(Move move);
}
