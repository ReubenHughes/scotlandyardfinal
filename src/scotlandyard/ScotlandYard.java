package scotlandyard;

import java.io.IOException;
import java.util.*;

/**
 * A class to perform all of the game logic.
 */

public class ScotlandYard implements ScotlandYardView, Receiver {

    protected MapQueue<Integer, Token> queue;
    protected Integer gameId;
    protected Random random  = new Random();
    protected Integer numberOfDetectives = 0;
    protected List<Spectator> spectators = new ArrayList<Spectator>();
    protected Integer round = 0;
    private Integer playersTurn = 0;
    protected List<Boolean> rounds;
    protected ScotlandYardGraph graph;
    protected Integer lastKnownMrXLocation = 0;
    protected Boolean playersWin = false;
    private Users players = new Users();

    /**
     * Constructs a new ScotlandYard object. This is used to perform all of the game logic.
     *
     * @param numberOfDetectives the number of detectives in the game.
     * @param rounds the List of booleans determining at which rounds Mr X is visible.
     * @param graph the graph used to represent the board.
     * @param queue the Queue used to put pending moves onto.
     * @param gameId the id of this game.
     */
    public ScotlandYard(Integer numberOfDetectives, List<Boolean> rounds, ScotlandYardGraph graph, MapQueue<Integer, Token> queue, Integer gameId) {
      this.gameId = gameId;
      this.numberOfDetectives = numberOfDetectives;
      this.rounds = rounds;
      this.graph = graph;
      this.queue = queue;
    }

    /**
     * Starts playing the game.
     */
    public void startRound() {
        if (isReady() && !isGameOver()) {
            turn();
        }
    }

    /**
     * Notifies a player when it is their turn to play.
     */
    public void turn() {
        Integer token = getSecretToken();
        queue.put(gameId, new Token(token, getCurrentPlayer(), System.currentTimeMillis()));
        notifyPlayer(getCurrentPlayer(), token);
    }

    /**
     * Plays a move sent from a player.
     *
     * @param move the move chosen by the player.
     * @param token the secret token which makes sure the correct player is making the move.
     */
    public void playMove(Move move, Integer token) {
        Token secretToken = queue.get(gameId);
        if (secretToken != null && token == secretToken.getToken()) {
            queue.remove(gameId);
            play(move);
            nextPlayer();
            startRound();
        }
    }

    /**
     * Returns a random integer. This is used to make sure the correct player
     * plays the move.
     * @return a random integer.
     */
    private Integer getSecretToken() {
        return random.nextInt();
    }

    /**
     * Notifies a player with the correct list of valid moves.
     *
     * @param colour the colour of the player to be notified.
     * @param token the secret token for the move.
     */
    private void notifyPlayer(Colour colour, Integer token) {
        updateLastKnownMrXLocation();
        players.setUser(colour);
        players.getPlayer().notify(players.getLocation(), validMoves(colour), token, this);
    }

    /**
     * Passes priority onto the next player whose turn it is to play.
     */
    protected void nextPlayer() {
        playersTurn = (playersTurn + 1) % (players.numberOfUsers());
    }

    /**
     * Allows the game to play a given move.
     *
     * @param move the move that is to be played.
     */
    protected void play(Move move) {
        if (move instanceof MoveTicket) play((MoveTicket) move);
        else if (move instanceof MoveDouble) play((MoveDouble) move);
        else if (move instanceof MovePass) play((MovePass) move);
    }

    /**
     * Plays a MoveTicket.
     *
     * @param move the MoveTicket to play.
     */
    protected void play(MoveTicket move) {
      players.setUser(move.colour);
      players.play(move);
      if (move.colour == Colour.Black)
      {
        round++;
        updateLastKnownMrXLocation();
      }
      for (Spectator spectator: spectators)
      {
        MoveTicket spectatedMove;
        if (move.colour == Colour.Black)
        {
          spectatedMove = MoveTicket.instance(move.colour, move.ticket, lastKnownMrXLocation);
        }
        else
        {
          spectatedMove = move;
        }
        spectator.notify(spectatedMove);
      }
    }

    /**
     * Plays a MoveDouble.
     *
     * @param move the MoveDouble to play.
     */
    protected void play(MoveDouble move) {
      for (Spectator spectator: spectators)
      {
        spectator.notify(move);
      }
      play(move.move1);
      play(move.move2);
      players.play(move);
    }

    /**
     * Plays a MovePass.
     *
     * @param move the MovePass to play.
     */
    protected void play(MovePass move) {
      for (Spectator spectator: spectators)
      {
        spectator.notify(move);
      }
    }

    /**
     * Returns the list of valid moves for a given player.
     *
     * @param player the player whose moves we want to see.
     * @return the list of valid moves for a given player.
     */
    public List<Move> validMoves(Colour player) {
      players.setUser(player);
      return players.getValidMoves(graph);
    }

    /**
     * Allows spectators to join the game. They can only observe as if they
     * were a detective: only MrX's revealed locations can be seen.
     *
     * @param spectator the spectator that wants to be notified when a move is made.
     */
    public void spectate(Spectator spectator) {
        spectators.add(spectator);
    }

    /**
     * Allows players to join the game with a given starting state. When the
     * last player has joined, the game must ensure that the first player to play is Mr X.
     *
     * @param player the player that wants to be notified when he must make moves.
     * @param colour the colour of the player.
     * @param location the starting location of the player.
     * @param tickets the starting tickets for that player.
     * @return true if the player has joined successfully.
     */
    public boolean join(Player player, Colour colour, int location, Map<Ticket, Integer> tickets) {
      PlayerData joiningPlayer = new PlayerData(player, colour, location, tickets);
      players.add(joiningPlayer);
      return true;
    }

    /**
     * A list of the colours of players who are playing the game in the initial order of play.
     * The length of this list should be the number of players that are playing,
     * the first element should be Colour.Black, since Mr X always starts.
     *
     * @return The list of players.
     */
    public List<Colour> getPlayers() {
        return players.getColours();
    }

    /**
     * Returns the colours of the winning players. If Mr X it should contain a single
     * colour, else it should send the list of detective colours
     *
     * @return A set containing the colours of the winning players
     */
    public Set<Colour> getWinningPlayers() {
      Set<Colour> winningPlayers = new HashSet<Colour>();
      if (isGameOver())
      {
        if (playersWin)
        {
          for (Colour player: players.getColours())
          {
            if (player != Colour.Black)
            {
              winningPlayers.add(player);
            }
          }
        }
        else
        {
          winningPlayers.add(Colour.Black);
        }
      }
        return winningPlayers;
    }

    /**
     * The location of a player with a given colour in its last known location.
     *
     * @param colour The colour of the player whose location is requested.
     * @return The location of the player whose location is requested.
     * If Black, then this returns 0 if MrX has never been revealed,
     * otherwise returns the location of MrX in his last known location.
     * MrX is revealed in round n when {@code rounds.get(n)} is true.
     */
    public int getPlayerLocation(Colour colour) {
      updateLastKnownMrXLocation();
      players.setUser(colour);
      if (colour == Colour.Black)
      {
          return lastKnownMrXLocation;
      }
      else
      {
          return players.getLocation();
      }
    }

    /**
     * Updates the last known location of Mr X, if the current round
     * is a round in which Mr X is to be revealed
     *
     */
    public void updateLastKnownMrXLocation()
    {
        if (rounds.get(getRound()))
        {
          players.setUser(Colour.Black);
          lastKnownMrXLocation = players.getLocation();
        }
    }

    /**
     * The number of a particular ticket that a player with a specified colour has.
     *
     * @param colour The colour of the player whose tickets are requested.
     * @param ticket The type of tickets that is being requested.
     * @return The number of tickets of the given player.
     */
    public int getPlayerTickets(Colour colour, Ticket ticket) {
      players.setUser(colour);
      return players.getNumberOfTickets(ticket);
    }

    /**
     * The game is over when MrX has been found or the agents are out of
     * tickets. See the rules for other conditions.
     *
     * @return true when the game is over, false otherwise.
     */
    public boolean isGameOver()
    {
        boolean gameOver = false;
        if (isReady()) {
            if (noRoundsLeft())
            {
                gameOver = true;
                playersWin = false;
            }
            else if (mrXOutOfMoves())
            {
                gameOver = true;
                playersWin = true;
            }
            else if (detectivesOutOfMoves())
            {
                gameOver = true;
                playersWin = false;
            }
            else if (detectivesCatchMrX())
            {
                gameOver = true;
                playersWin = true;
            }
        }
        return gameOver;
    }

    /**
    * Checks if mr X has no valid moves.
    * @return True if mr X has no valid moves on his turn.
    */
    private boolean mrXOutOfMoves()
    {
      if (getCurrentPlayer() == Colour.Black && validMoves(Colour.Black).size() == 0) { return true; }
      return false;
    }

    /**
    * Checks if game has run out of rounds after all detectives have taken
    * their turns.
    *
    * @return True if no rounds remain and detectives have taken respective
    * turns in final round.
    */
    private boolean noRoundsLeft()
    {
      boolean outOfRounds = false;
      if (getRound() + 1 == getRounds().size() && getCurrentPlayer() == Colour.Black)
      {
        outOfRounds = true;
      }
      return outOfRounds;
    }

    /**
     * Checks if all the detectives passes.
     *
     * @returns True if all the detectives passes.
     */
    private boolean detectivesOutOfMoves()
    {
      int movePassed = 0;
      for (Colour player : players.getColours())
      {
        players.setUser(player);
        if (players.getValidMoves(graph).get(0) == MovePass.instance(player)) { movePassed++; }
      }
      if (movePassed == numberOfDetectives)
      {
        return true;
      }
      return false;
    }

    /**
     * Checks if the detectives have caught Mr X.
     *
     * @returns True if any of the detectives is in the same location as Mr X.
     */
    private boolean detectivesCatchMrX()
    {
      boolean mrXCaught = false;
      players.setUser(Colour.Black);
      Integer mrXLocation = players.getLocation();
      for (Colour player : players.getColours())
      {
        if (player != Colour.Black)
        {
          players.setUser(player);
          if (mrXLocation == players.getLocation())
          {
            mrXCaught = true;
          }
        }
      }
      return mrXCaught;
    }

    /**
     * A game is ready when all the required players have joined.
     *
     * @return true when the game is ready to be played, false otherwise.
     */
    public boolean isReady() {
      if (numberOfDetectives + 1 == players.numberOfUsers())
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    /**
     * The player whose turn it is.
     *
     * @return The colour of the current player.
     */
    public Colour getCurrentPlayer() {
      players.setUser(playersTurn);
      return players.getColour();
    }

    /**
     * The round number is determined by the number of moves MrX has played.
     * Initially this value is 0, and is incremented for each move MrX makes.
     * A double move counts as two moves.
     *
     * @return the number of moves MrX has played.
     */
    public int getRound() {
        return round;
    }

    /**
     * A list whose length-1 is the maximum number of moves that MrX can play in a game.
     * The getRounds().get(n) is true when MrX reveals the target location of move n,
     * and is false otherwise.
     * Thus, if getRounds().get(0) is true, then the starting location of MrX is revealed.
     *
     * @return a list of booleans that indicate the turns where MrX reveals himself.
     */
    public List<Boolean> getRounds() {
      return rounds;
    }

}
