package scotlandyard;

import java.util.List;
import java.util.ArrayList;

/**
 * This is the composite in the composite design pattern.
 */
public class Users implements User {
  private List<User> players;
  private User player;

  /**
   * Creates a new Users object.
   */
  public Users()
  {
    players = new ArrayList<User>();
  }

  /** Adds a player to the list of users in the game.
   *
   * @param player the player that wishes to be added.
     */
  public void add(User player)
  {
    this.player = player;
    if (player.getColour() == Colour.Black)
    {
      players.add(0, player);
    }
    else
    {
      players.add(player);
    }
  }

  /**
   * Return a Player belonging the player object.
   *
   * @return the player belonging to the player object
     */
  public Player getPlayer() { return player.getPlayer(); }


  /**
   * Returns the number of users playing the game.
   * @return Returns the number of users.
     */
  public Integer numberOfUsers() { return players.size(); }

  /**
   * Sets a sepecific User from the players list to player.
   *
   * @param index the index of the player in the list to set as player.
     */
  public void setUser(Integer index) { player = players.get(index); }

  /**
   * Sets a sepecific User from the players list to player.
   *
   * @param colour the colour of the player in the list to set as player.
     */
  public void setUser(Colour colour)
  {
    for (User player: players)
    {
      if (player.getColour() == colour)
      {
        this.player = player;
      }
    }
  }

  /**
   * Returns the colour of player.
   * @return Returns the colour of player.
     */
  public Colour getColour() { return player.getColour(); }

  /** A list of colours of the current players will be returned.
   *
   * @return Returns the list of colours used in the players list.
     */
  public List<Colour> getColours()
  {
    List<Colour> colours = new ArrayList<Colour>();
    for (User player: players)
    {
      colours.add(player.getColour());
    }
    return colours;
  }

  /** An integer of player location will be returned.
   *
   * @return an integer representing the location of player.
     */
  public Integer getLocation() { return player.getLocation(); }

  /** Set the location of player.
   *
   * @param location the integer to represent the location of player.
     */
  public void setLocation(Integer location)
  {
    players.get(players.indexOf(player)).setLocation(location);
    player.setLocation(location);
  }

  /**
   * Returns the valid moves for player.
   * @param graph ScotlandYardGraph representing the nodes and edges for the game.
   * @return the list of valid moves that is abled to be played by player.
     */
  public List<Move> getValidMoves(ScotlandYardGraph graph)
  {
    List<Move> moves = player.getValidMoves(graph);
    Integer target;
    List<Move> allPossibleMoves = new ArrayList<Move>();
    List<Integer> playersLocations = getPlayersLocations();
    for (Move move: moves)
    {
      if (move instanceof MoveTicket)
      {
        MoveTicket moveTicket = (MoveTicket) move;
        if (!playersLocations.contains(moveTicket.target))
        {
          allPossibleMoves.add(move);
        }
      }
      if (move instanceof MoveDouble)
      {
        MoveDouble moveDouble = (MoveDouble) move;
        if (!playersLocations.contains(moveDouble.move1.target) && !playersLocations.contains(moveDouble.move2.target))
        {
          allPossibleMoves.add(moveDouble);
        }
      }
      if (move instanceof MovePass)
      {
        allPossibleMoves.add(move);
      }
    }
    return allPossibleMoves;
  }

  /** Finds the number of tickets for a specific ticket.
   *
   * @param ticket The amount of said ticket type requested.
   * @return the number of tickets of the requested ticket type.
     */
  public Integer getNumberOfTickets(Ticket ticket) { return player.getNumberOfTickets(ticket); }

  private List<Integer> getPlayersLocations()
  {
    List<Integer> locations = new ArrayList<Integer>();
    for (User user: players)
    {
      if (user != this.player && user.getColour() != Colour.Black)
      {
        locations.add(user.getLocation());

      }
    }
    return locations;
  }

  /**
   * When player plays a move.
   *
   * @param move the specific move to be played.
     */
  public void play(Move move)
  {
    if (move instanceof MoveTicket)
    {
      player.play(move);
    }
    else if (move instanceof MoveDouble)
    {
      player.play(move);
    }
    addTicketsToMrX(move);
  }

  /** Adds a ticket to mrX when the detectives have used a ticket.
   *
   * @param move the ticket used by detectives.
     */
  private void addTicketsToMrX(Move move)
  {
    if (move.colour != Colour.Black)
    {
      if (move instanceof MoveTicket)
      {
        MoveTicket moveTicket = (MoveTicket) move;
        PlayerData mrX = (PlayerData) players.get(0);
        mrX.addTicket(moveTicket.ticket);
        player = mrX;
        players.set(0,player);
      }
      else if (move instanceof MoveDouble)
      {
        MoveDouble moveDouble = (MoveDouble) move;
        PlayerData mrX = (PlayerData) players.get(0);
        mrX.addTicket(moveDouble.move1.ticket);
        mrX.addTicket(moveDouble.move2.ticket);
        player = mrX;
        players.set(0,player);
      }
    }
  }
}
